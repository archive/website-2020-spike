from django.urls import reverse

from website.common.tests import BaseTestCase


class BlogListViewTestCase(BaseTestCase):
    def test_accessible(self):
        response = self.client.get(reverse("blog:list"))
        self.assertEqual(response.status_code, 200)
