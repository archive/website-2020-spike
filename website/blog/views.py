from django.templatetags.static import static
from django.views.generic import TemplateView


class BlogListView(TemplateView):
    template_name = "blog/index.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["header_image"] = static("img/header.jpg")
        return context
