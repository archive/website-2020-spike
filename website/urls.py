import debug_toolbar
from django.conf import settings
from django.urls import include, path

from website.common.urls import urlpatterns as common_urlpatterns

urlpatterns = [
    path("", include(common_urlpatterns)),
    path("blog/", include("website.blog.urls")),
]

if settings.DEBUG:
    urlpatterns.append(path("__debug__/", include(debug_toolbar.urls)))
