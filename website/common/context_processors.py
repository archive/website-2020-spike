from django.conf import settings
from django.utils.text import slugify


def settings_injector(request):
    return {"settings": settings}


def view_name(request):
    return {"view_name": slugify(request.resolver_match.func.__name__)}
