from bs4 import BeautifulSoup
from django.test import SimpleTestCase
from django.urls import reverse


class BaseTestCase(SimpleTestCase):
    pass


class HomepageViewTestCase(BaseTestCase):
    def test_accessible(self):
        response = self.client.get(reverse("homepage"))
        self.assertEqual(response.status_code, 200)

    def test_contains_view_name(self):
        response = self.client.get(reverse("homepage"))
        self.assertContains(response, '<body class="homepageview">')

    def test_navbar_links_accessible(self):
        response = self.client.get(reverse("homepage"))
        soup = BeautifulSoup(response.content, features="html.parser")
        nav_links = list(soup.find("nav").find_all("a", class_="nav-link"))
        self.assertEqual(len(nav_links), 2)
        for nav_link in nav_links:
            nav_link_response = self.client.head(nav_link.attrs["href"])
            self.assertEqual(nav_link_response.status_code, 200)

    def test_navbar_link_text(self):
        response = self.client.get(reverse("homepage"))
        soup = BeautifulSoup(response.content, features="html.parser")
        nav_links = list(soup.find("nav").find_all("a", class_="nav-link"))
        self.assertEqual(
            {nav_link.find("code").text for nav_link in nav_links},
            {"~/about", "~/blog"},
        )


class AboutViewTestCase(BaseTestCase):
    def test_accessible(self):
        response = self.client.get(reverse("about"))
        self.assertEqual(response.status_code, 200)
