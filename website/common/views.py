from django.templatetags.static import static
from django.views.generic import TemplateView


class HomepageView(TemplateView):
    template_name = "homepage.html"


class AboutView(TemplateView):
    template_name = "about.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["header_image"] = static("img/header.jpg")
        return context
