from django import template

register = template.Library()


@register.filter(is_safe=True)
def to_navbar_text(path):
    return "~/" + path.strip("/")
