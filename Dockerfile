FROM node:lts-alpine as build

ENV NODE_ENV production
ENV NPM_CONFIG_PROUCTION false

WORKDIR /app

COPY . /app

RUN ./scripts/setup-node.sh

FROM python:3.8-slim

ENV DEBUG false

WORKDIR /app
COPY . /app

COPY --from=build /app/static/build /app/static/build

RUN ./scripts/setup-python.sh

RUN rm -rf ./static

EXPOSE 8000
CMD ["gunicorn", "-c", "website/gunicorn.conf.py", "--access-logfile", "/dev/stdout", "website.wsgi:application"]
