#!/usr/bin/env bash

set -ex

pip install -r requirements.txt

if [ "$DEBUG" != "false" ];
then
  pip install -r dev-requirements.txt
fi

python3 ./manage.py collectstatic --noinput --clear -v 2
