#!/usr/bin/env sh

export PATH=node_modules/.bin:${PATH}

set -ex

npm ci

./scripts/build-static.sh
