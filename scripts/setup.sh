#!/usr/bin/env bash

set -e


./scripts/setup-node.sh

export PATH=env/bin:${PATH}

python3 -m venv env

./scripts/setup-python.sh
