#!/usr/bin/env bash


export PATH=env/bin:node_modules/.bin:${PATH}

set -ex

coverage run --source=website manage.py test $@

coverage report
coverage html

black website/ --check

flake8 website/

isort -rc -c website/

mypy website/

eslint static/src/js

gunicorn --check-config -c website/gunicorn.conf.py website.wsgi:application
