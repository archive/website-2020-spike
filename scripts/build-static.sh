#!/usr/bin/env sh

export PATH=node_modules/.bin:${PATH}

set -ex

rm -rf static/build

mkdir -p static/build/css static/build/js

parcel build --no-autoinstall --no-source-maps -d static/build/js static/src/js/index.js

# Separate step because parcel process images itself, and breaks them
sass -I node_modules/ -s compressed --no-source-map static/src/scss/index.scss static/build/css/index.css

# Install bootstrap-reboot
cp node_modules/bootstrap/dist/css/bootstrap-reboot.min.css static/build/css/bootstrap-reboot.min.css

# Install materialize CSS
cp node_modules/materialize-css/dist/js/materialize.min.js static/build/js/materialize.min.js

# Install fontawesome
cp node_modules/@fortawesome/fontawesome-free/css/all.min.css static/build/css/font-awesome.min.css
cp -r node_modules/@fortawesome/fontawesome-free/webfonts static/build/

# Install images
cp -r static/src/img static/build/
