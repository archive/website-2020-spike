#!/usr/bin/env bash

set -e

export PATH=env/bin:${PATH}

export DEBUG=true

./manage.py runserver
